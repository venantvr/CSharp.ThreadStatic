﻿/*
 * Created by SharpDevelop.
 * User: Rénald
 * Date: 15/02/2014
 * Time: 09:29
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */

using System;
using System.Threading.Tasks;

namespace ThreadStatic
{
    internal class Program
    {
        [ThreadStatic] public static int AccessCounter;
        [ThreadStatic] public static Custom Custom;

        public static void Main(string[] args)
        {
            Custom = new Custom();

            for (var i = 0; i < 100000; i++)
            {
                Custom.Add();
                ++AccessCounter;
            }

            var taskA = new Task(() =>
                                 {
                                     while (AccessCounter < 5000000)
                                     {
                                         AccessCounter++;
                                     }
                                 }
                );

            var taskB = new Task(() =>
                                 {
                                     while (AccessCounter < 5000000)
                                     {
                                         AccessCounter++;
                                     }
                                 }
                );

            taskA.Start();
            taskB.Start();

            taskA.Wait();
            taskB.Wait();

            Console.WriteLine(AccessCounter);
            Console.WriteLine(Custom.AccessCounter);
        }
    }
}